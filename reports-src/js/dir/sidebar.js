angular
    .module('Taxi')
    .directive('sidebar', function ($timeout, $translate) {
        return {
            restrict: "C",
            templateUrl: '/reports/html/share/sidebar.html',
            link: function ($scope) {
                $scope.items = [
                    {icon: 'tasks', title: 'Справочники', items: [
                        {sref: 'drivers', title: 'Водители'},
                        {sref: 'verification', title: 'Верификация'},
                        {sref: 'cars', title: 'Машины'},
                        {sref: 'filials', title: 'Филиалы', disabled: true},
                        {sref: 'legal', title: 'Юр. лица'},
                        {sref: 'clients', title: 'Клиенты'},
                    ]},
                    {icon: 'th', title: 'Отчёты', items: [
                        {sref: 'reports({report:"drivers"})', icon: 'list-alt', title: 'Список водителей', disabled: true},
                        {sref: 'reports({report:"orders"})', icon: 'list-alt', title: 'По заказам'},
                        {sref: 'reports({report:"driversSum"})', icon: 'list-alt', title: 'По водителям сводный'},
                        {sref: 'reports({report:"managerSum"})', icon: 'list-alt', title: 'По диспетчерам сводный'},
                        {sref: 'reports({report:"car"})', icon: 'list-alt', title: 'По ТС сводный'},
                        {sref: 'reports({report:"orderSum"})', icon: 'list-alt', title: 'Заказы сводный (по сменам)'},
                        {sref: 'reports({report:"cancelSum"})', icon: 'list-alt', title: 'По отменам'},
                        {sref: 'reports({report:"legalSum"})', icon: 'list-alt', title: 'По юр. лицам сводный'},
                        {sref: 'reports({report:"clientsSum"})', icon: 'list-alt', title: 'По клиентам сводный'},
                        {sref: 'reports({report:"partnersSum"})', icon: 'list-alt', title: 'По партнёрам сводный'},
                    ]},
                ];

                $scope.expand = function (item) {
                    if (item.expanded) return item.expanded = false;
                    angular.forEach($scope.items, function(item){
                        item.expanded = false;
                    });
                    item.expanded = true;
                };

                $timeout(function(){
                    // При старте нужно раскрыть 2-й уровень меню, в котором активная ссылка.
                    // TODO: запилить директиву активного второго уровня меню в сайдбаре
                    $('#sidebar > ul > li > ul > li > a.active')
                        .closest('ul')
                        .show()
                        .prev('a')
                        .addClass('active')
                });
            },
        };
    });
