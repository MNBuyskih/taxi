angular
    .module('Taxi', [
        'ui.router',
        'pascalprecht.translate',
        'datatables',
    ])
    .config(function($stateProvider, $urlRouterProvider, $translateProvider){
        $stateProvider
            .state('index', {
                abstract: true,
                templateUrl: '/reports/html/share/abstract.html',
            })
            .state('reports', {
                url: "/reports/:report",
                templateUrl: '/reports/html/reports/index.html',
                controller: 'ReportsController'
            });

        $urlRouterProvider.otherwise('/reports/orders');

        $translateProvider.useStaticFilesLoader({
            prefix: 'lang/',
            suffix: '.json'
        });
        $translateProvider.preferredLanguage('ru');
        $translateProvider.useSanitizeValueStrategy('escaped');
    })
    .run(function($rootScope, $state, $translate){
        // TODO: Тут надо проверку на авторизацию
        $rootScope.$state = $state;

        // TODO: Вынести переключалку языка в директиву
        $rootScope.lang = {
            use: 'ru',
            changed: undefined,
            langs: [
                {key: 'ru', lang: 'RU'},
                {key: 'ro', lang: 'RO'},
                {key: 'en', lang: 'EN'},
            ]
        };
        $rootScope.$watch('lang.use', function(use){
            $translate
                .use(use)
                .then(function(){
                    $rootScope.lang.changed = !$rootScope.lang.changed;
                });
        });
    });
