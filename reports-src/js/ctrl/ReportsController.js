angular
    .module('Taxi')
    .controller('ReportsController', function($scope, $rootScope, $state, Reports, DTOptionsBuilder, DTColumnBuilder, ColumnsParser, ColumnsAdatapter){
        var reporter = Reports[$state.params.report];

        // 4. Инициализируем таблицу

        reporter
            // 1. Получаем конфиги колонок.
            .columns()
            .then(function(response){
                var columns = response.data;

                // 2. Парсим их и перерисовывем шапку таблицы.
                ColumnsParser($('#reports-table'), columns);

                // 3. Парсим конфиги и генерим из них удобоваримый формат для DataTable
                var config = ColumnsAdatapter(columns);
            });

        // // Общие настройки таблицы устанавливаем тут.
        // // Настройки колонок приходят с сервера.
        // $scope.dtOptions = DTOptionsBuilder
        //     .fromFnPromise(reporter.data)
        //     .withOption('scrollX', true)
        //     .withOption('bPaginate', false);
        // $scope.dtColumns = reporter.columns();
        // $scope.dtInstance = {};

        $scope.reload = function(){
            if (!$scope.dtInstance.reloadData) return;
            $scope.dtInstance.reloadData();
        };

        $rootScope.$watch('lang.changed', function(){
            if (!$scope.dtInstance.rerender) return;
            $scope.dtColumns = reporter.columns();
            $scope.dtInstance.rerender();
        });
    });
