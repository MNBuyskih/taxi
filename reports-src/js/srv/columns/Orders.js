angular
    .module('Taxi')
    .service('OrdersColumns', function ($filter) {
        return function () {
            var columns = [
                {
                    data: "DriverLogin",
                    defaultContent: "-",
                    title: $filter("translate")("Позывной"),
                    danger: true
                },
                {
                    data: "DriverDisplayName",
                    defaultContent: "-",
                    title: $filter("translate")("ФИО"),
                    width: 100
                },
                {
                    data: "OrderId",
                    defaultContent: "-",
                    title: $filter("translate")("Номер"),
                    "class": "hidden"
                },
                {
                    data: "OrderComments",
                    defaultContent: "-",
                    title: $filter("translate")("Заказ"),
                    width: 300
                },
                {
                    data: "OrderCreatedDateTime",
                    defaultContent: "-",
                    title: $filter("translate")("Создан"),
                    width: 150,
                    format: function(value){
                        return $filter('date')(value, 'yyyy.MM.dd HH:mm:ss');
                    },
                },
                {
                    data: '',
                    title: 'Тариф',
                    defaultContent: '-',
                    colspan: 2,
                },
                {
                    data: "OrderTariffName",
                    defaultContent: "-",
                    title: $filter("translate")("Тариф"),
                    width: 150,
                    format: "\"%s%\"",
                    success: true
                },
                {
                    data: "OrderPaymentType",
                    defaultContent: "-",
                    title: $filter("translate")("Оплата")
                },
                {
                    data: "OrderProvider",
                    defaultContent: "-",
                    title: $filter("translate")("Тип"),
                    warning: true
                },
                {
                    data: "OrderCost",
                    defaultContent: "-",
                    title: $filter("translate")("Сумма"),
                    page: true,
                    total: false,
                    width: 80,
                    format: "%s%р.",
                    info: true,
                },
                {
                    data: "OrderFee",
                    defaultContent: "-",
                    title: $filter("translate")("Комиссия"),
                    page: true,
                    total: false,
                    width: 80,
                    format: "%s%р.",
                    primary: true,
                },
                {
                    data: "DriverSubscriptionTypeName",
                    defaultContent: "-",
                    title: $filter("translate")("Условия работы")
                },
                {
                    data: "OrderSource",
                    defaultContent: "-",
                    title: $filter("translate")("Адрес подачи"),
                    width: 250
                },
                {
                    data: "OrderDestination",
                    defaultContent: "-",
                    title: $filter("translate")("Адрес назначения"),
                    width: 250
                },
                {
                    data: "ClientPhone",
                    defaultContent: "-",
                    title: $filter("translate")("Телефон"),
                    width: 200
                },
                {
                    data: "OrderAssignedDateTime",
                    defaultContent: "-",
                    title: $filter("translate")("Принят"),
                    width: 150,
                    format: function(value){
                        return $filter('date')(value, 'yyyy.MM.dd HH:mm:ss');
                    },
                },
                {
                    data: "OrderWaitingDateTime",
                    defaultContent: "-",
                    title: $filter("translate")("На месте"),
                    width: 150,
                    format: function(value){
                        return $filter('date')(value, 'yyyy.MM.dd HH:mm:ss');
                    },
                },
                {
                    data: null,
                    defaultContent: "-",
                    title: $filter("translate")("Отзвон"),
                    width: 150
                },
                {
                    data: "OrderTransportingDateTime",
                    defaultContent: "-",
                    title: $filter("translate")("В пути"),
                    width: 150,
                    format: function(value){
                        return $filter('date')(value, 'yyyy.MM.dd HH:mm:ss');
                    },
                },
                {
                    data: "OrderCompleteDateTime",
                    defaultContent: "-",
                    title: $filter("translate")("Завершен"),
                    width: 150,
                    format: function(value){
                        return $filter('date')(value, 'yyyy.MM.dd HH:mm:ss');
                    },
                }
            ];

            return columns.map(function(col){
                var addClass = function(className){
                    classNames = col.className ? col.className.split(' ') : [];
                    classNames.push(className);
                    col.className = classNames.join(' ');
                };

                ['danger', 'warning', 'info', 'primary'].forEach(function(className){
                    if(col[className]) addClass(className);
                });

                if (col.format) {
                    if (angular.isFunction(col.format)) {
                        col.render = (function(format){
                            return function(data){
                                return format(data);
                            };
                        })(col.format);
                    } else {
                        col.render = (function(format){
                            return function(data){
                                return format.replace('%s%', data);
                            };
                        })(col.format);
                    }
                }

                return col;
            });
        };
    });
