angular
    .module('Taxi')
    .service('ColumnsParser', function(){
        return function (table, config) {
            var trs = [];

            function createTr(){
                return $('<tr></tr>');
            }

            function createTd(){
                var td = $('<th></th>');
                td.attr({rowspan: 1, colspan: 1});
                return td;
            }

            function getTr(n){
                if(!trs[n]) trs[n] = createTr();
                return trs[n];
            }

            function isCell(group) {
                return group.Header.Text != 'UnGroup';
            }

            function setHeader(td, group){
                if(isCell(group)) {
                    td.html(group.Header.Text);
                } else {
                    td.attr('rowspan', +td.attr('rowspan') + 1);
                }
            }

            function setColspan(td, group) {
                var findColumnLists = function (group) {
                    var i = 0;
                    if (group.GroupList) {
                        group.GroupList.forEach(function(gr){
                            i += findColumnLists(gr);
                        });
                    }

                    if(group.ColumnList) {
                        i += group.ColumnList.length;
                    }

                    return i;
                };

                td.attr('colspan', findColumnLists(group));
            }

            function parseCol(col, trN, td) {
                trN = trN || 0;
                var tr = getTr(trN);
                td = td || createTd();
                td.html(col.Header.Text);
                tr.append(td);
            }

            function parseGroup(group, trN, td){
                trN = trN || 0;
                if (!td) {
                    td = createTd();
                    setColspan(td, group);
                }

                var tr = getTr(trN);
                setHeader(td, group);

                if(group.GroupList){
                    group.GroupList.forEach(function(gr){
                        parseGroup(gr, trN + 1, isCell(group) ? undefined : td);
                    });
                }

                if (group.ColumnList) {
                    group.ColumnList.forEach(function(col){
                        parseCol(col, trN + 1, isCell(group) ? undefined : td);
                    });
                }

                if(isCell(group) || trN === 0){
                    tr.append(td);
                }
            }

            config.GroupList.forEach(function(group){
                parseGroup(group);
            });

            var thead = $('<thead></thead>');
            table.append(thead);
            trs.forEach(function(tr){
                thead.append(tr);
            });
        };
    });
