var TablHeadRenderer = function () {
    var config = {};
    var trs = [];

    function createTr() {
        return $('<tr></tr>');
    }

    function createTd() {
        var td = $('<td></td>');
        td.attr({
            rowspan: 1,
            colspan: 1
        });
        return td;
    }

    function getTr(n) {
        if (!trs[n]) trs[n] = createTr();
        return trs[n];
    }

    function isCell(group) {
        return !group.IsUngroup;
    }

    function setHeader(td, group) {
        if (isCell(group)) {
            td.html(group.Caption.Text);
        } else {
            td.attr('rowspan', +td.attr('rowspan') + 1);
        }
    }

    function findGroupLists(group) {
        return (group.GroupList || []).reduce(function (i, gr) {
            i += calcColspan(gr);
            return i;
        }, 0);
    }

    function findColumnLists(group) {
        return (group.ColumnList && group.ColumnList.length) || 0;
    }

    function calcColspan(group) {
        return findGroupLists(group) + findColumnLists(group);
    }

    function setColspan(td, group) {
        td.attr('colspan', calcColspan(group));
        return td;
    }

    function parseCol(col, trN, td) {
        var tr = getTr(trN || 0);
        td = td || createTd();
        td.html(col.Caption.Text);
        setStyles(td, col);
        tr.append(td);
    }

    function parseGroup(group, trN, td) {
        trN = trN || 0;

        td = td || setColspan(createTd(), group);
        setHeader(td, group);

        var tr = getTr(trN),
            cell = isCell(group),
            parseGroupItem = function (col, method) {
                if (group[col]) {
                    group[col] = sort(group[col]);
                    group[col].forEach(function (gr) {
                        method(gr,  trN + 1, cell ? undefined : td);
                    });
                }
            };

        parseGroupItem('GroupList', parseGroup);
        parseGroupItem('ColumnList', parseCol);

        if (cell || trN === 0) {
            setStyles(td, group);
            tr.append(td);
        }
    }

    function setStyles(td, group) {
        var vAlignMap = {
            Center: 'middle'
        };

        td.css({
            'background-color': group.Caption.BackColor,
            'color': group.Caption.ForeColor,
            'text-align': group.Caption.HorizontalAligment,
            'vertical-align': vAlignMap[group.Caption.VerticalAligment]
        });
        return td;
    }

    function sort(arr) {
        return arr.sort(function (a, b) {
            return a.Order - b.Order;
        });
    }

    return {
        setConfig: function (c) {
            config = c;
            return this;
        },
        render: function (table) {
            config.GroupList.forEach(function (group) {
                parseGroup(group);
            });
            trs.forEach(function (tr) {
                table.append(tr);
            });
            return this;
        }
    };
};
