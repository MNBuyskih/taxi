angular
    .module('Taxi')
    .service('Reports', function ($http, $q, OrdersColumns) {
        // Тут должна быть какакая-то функция, которая будет работать со сгенерированными данными.
        // Пока что работаем с обычным $http

        return {
            'orders': {
                data: function () {
                    return $http
                        .get('/reports/dumyData/orders.json')
                        .then(function(response){
                            return response['data']['Result'];
                        });
                },
                columns: function () {
                    return $http
                        .get('http://ddmdtaxiclub.smarttaxi.ru:2087/ReportSettings/GetReportSettingsByType');
                }
            }
        };
    });
