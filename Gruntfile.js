module.exports = function(grunt) {
    grunt.initConfig({
        watch: {
            js: {
                files: ['reports-src/**/*.js'],
                tasks: 'js',
                options: {spawn: false},
            },
            html: {
                files: ['reports-src/**/*.html'],
                tasks: 'newer:copy:html',
                options: {spawn: false},
            },
            json: {
                files: ['reports-src/**/*.json'],
                tasks: 'newer:minjson',
                options: {spawn: false},
            },
        },
        browserSync: {
            bsFiles: {
                src: [
                    'public/**/*.js',
                    'public/**/*.html',
                ],
            },
            options: {
                server: {
                    baseDir: "./public",
                    directory: true
                },
                watchTask: true,
            }
        },

        copy: {
            html: {
                expand: true,
                cwd: 'reports-src/',
                src: ['**/*.html'],
                dest: 'public/reports/'
            },
            json: {
                expand: true,
                cwd: 'reports-src/',
                src: ['**/*.json'],
                dest: 'public/reports/'
            },
        },

        minjson: {
            all: {
                expand: true,
                src: ['**/*.json'],
                dest: 'public/reports/',
                ext: '.json',
                cwd: 'reports-src/'
            }
        },

        concat: {
            js: {
                src: 'reports-src/js/**/*.js',
                dest: 'public/reports/js/reports.js',
                options: {
                    sourceMap: true,
                    sourceMapName: 'public/reports/js/reports.js.map'
                }
            },
            vendors: {
                src: [
                    'bower_components/jquery/dist/jquery.min.js',
                    'bower_components/angular/angular.min.js',
                    'bower_components/angular-ui-router/release/angular-ui-router.min.js',
                    'bower_components/angular-translate/angular-translate.min.js',
                    'bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js',
                    'bower_components/angular-translate-loader-url/angular-translate-loader-url.min.js',
                    'bower_components/datatables/media/js/jquery.dataTables.min.js',
                    'bower_components/angular-datatables/dist/angular-datatables.min.js',
                ],
                dest: 'public/reports/js/vendors.js'
            }
        },
        ngAnnotate:{
            js: {
                files: {'<%= concat.js.dest %>': '<%= concat.js.dest %>'}
            }
        },
        uglify: {
            js: {
                options: {
                    sourceMap: true,
                    sourceMapIn: '<%= concat.js.options.sourceMapName %>'
                },
                src: ['<%= concat.js.dest %>', '<%= ngtemplates.app.dest %>'],
                dest: 'public/reports/js/reports.min.js',
            }
        },
        i18nextract: {
            default: {
                src: ['reports-src/**/*.js', 'reports-src/**/*.html'],
                lang: ['en', 'ro', 'ru'],
                safeMode: true,
                dest: 'reports-src/lang',
                defaultLang: 'ru',
                stringifyOptions: true,
            }
        },
        ngtemplates: {
            app: {
                cwd: 'reports-src',
                src: 'html/**/*.html',
                dest: 'public/reports/js/templates.js',
                options: {
                    module: 'Taxi',
                    prefix: '/reports/',
                    htmlmin: {
                        collapseWhitespace: true,
                        conservativeCollapse: false,
                        collapseBooleanAttributes: true,
                        removeComments: true,
                        removeAttributeQuotes: true,
                        removeRedundantAttributes: true,
                        removeEmptyAttributes: true,
                        minifyCSS: true,
                    }
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-angular-translate');
    grunt.loadNpmTasks('grunt-angular-templates');
    grunt.loadNpmTasks('grunt-minjson');
    grunt.loadNpmTasks('grunt-newer');

    grunt.registerTask('js', ['concat:js', 'ngAnnotate:js', 'uglify:js']);
    grunt.registerTask('translate', ['i18nextract']);

    grunt.registerTask('build', ['concat', 'ngtemplates', 'ngAnnotate', 'uglify', 'copy', 'minjson']);
    grunt.registerTask('dev', ['concat', 'ngAnnotate', 'uglify', 'copy', 'minjson', 'browserSync', 'watch']);
};
